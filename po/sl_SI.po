# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Dušan <dusan.k@zoho.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-01-14 21:20+0100\n"
"PO-Revision-Date: 2015-02-12 14:52+0000\n"
"Last-Translator: runasand <runa.sandvik@gmail.com>\n"
"Language-Team: Slovenian (Slovenia) (http://www.transifex.com/projects/p/"
"torproject/language/sl_SI/)\n"
"Language: sl_SI\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "Tor je pripravljen"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "Sedaj lahko dostopate do omrežja"

#: config/chroot_local-includes/etc/whisperback/config.py:69
#, fuzzy, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Pomagajte nam popraviti vašega hrošča! </h1>\n"
"<p>Preberite<a href=\"%s\">naša navodila za poročanje o hrošču</a>.</p>\n"
"<p><strong>Ne vključujte več osebnih podatkov kot je\n"
"potrebno!</strong></p>\n"
"<h2>O dajanju naslova e-pošte</h2>\n"
"Če vam ni odveč odkriti nekaj delčkov svoje identitete\n"
"razvijalcem Sledi, nam lahko podate svoj e-naslov,\n"
"da vas vprašamo o podrobnostih hrošča. Dodan\n"
"javni PGP ključ nam pomaga dešifrirati podobna bodoča\n"
"sporočila.</p>\n"
"<p>Vsakdo, ki vidi ta odgovor, bo verjetno zaključil, da ste\n"
"uporabnik Sledi. Čas je, da se vprašamo koliko zaupamo našim\n"
"Internet in poštnim ponudnikom?</p>\n"

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:51
msgid ""
"You can install additional software automatically from your persistent "
"storage when starting Tails."
msgstr ""

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:77
msgid ""
"The following software is installed automatically from your persistent "
"storage when starting Tails."
msgstr ""

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:132
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:172
msgid ""
"To add more, install some software using <a href=\"synaptic.desktop"
"\">Synaptic Package Manager</a> or <a href=\"org.gnome.Terminal.desktop"
"\">APT on the command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/share/tails/additional-software/configuration-window.ui:151
msgid "_Create persistent storage"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:57
msgid "Persistence is disabled for Electrum"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:59
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:60
msgid "Do you want to start Electrum anyway?"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/electrum:63
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Zagon"

#: config/chroot_local-includes/usr/local/bin/electrum:64
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "_Izhod"

#: config/chroot_local-includes/usr/local/bin/keepassx:17
#, sh-format
msgid ""
"<b><big>Do you want to rename your <i>KeePassX</i> database?</big></b>\n"
"\n"
"You have a <i>KeePassX</i> database in your <i>Persistent</i> folder:\n"
"\n"
"<i>${filename}</i>\n"
"\n"
"Renaming it to <i>keepassx.kdbx</i> would allow <i>KeePassX</i> to open it "
"automatically in the future."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/keepassx:25
msgid "Rename"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/keepassx:26
msgid "Keep current name"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/replace-su-with-sudo:21
msgid "su is disabled. Please use sudo instead."
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:75
msgid "Restart"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:78
msgid "Lock screen"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:81
msgid "Power Off"
msgstr "Izklop"

#: config/chroot_local-includes/usr/local/bin/tails-about:22
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Sledi"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Vizitka Sledi"

#: config/chroot_local-includes/usr/local/bin/tails-about:35
msgid "The Amnesic Incognito Live System"
msgstr "aktivni sistem The Amnesic Incognito"

#: config/chroot_local-includes/usr/local/bin/tails-about:36
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Oblikuje informacije\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:54
msgid "not available"
msgstr "ni primeren"

#. Translators: Don't translate {details}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:147
#, fuzzy, python-brace-format
msgid ""
"{details} Please check your list of additional software or read the system "
"log to understand the problem."
msgstr ""
"Nadgradnja ni uspela. To je lahko zaradi težav v omrežju. Preverite omrežno "
"povezavo, poskusite ponovno zagnati Sledi, ali pa preberite sistemski "
"dnevnik za boljše razumevanje problema."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:152
msgid ""
"Please check your list of additional software or read the system log to "
"understand the problem."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:156
msgid "Show Log"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:156
msgid "Configure"
msgstr ""

#. Translators: Don't translate {beginning} or {last}, they are
#. placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:222
#, python-brace-format
msgid "{beginning} and {last}"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:223
msgid ", "
msgstr ""

#. Translators: Don't translate {packages}, it's a placeholder and will
#. be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:289
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:319
#, fuzzy, python-brace-format
msgid "Add {packages} to your additional software?"
msgstr "Vaša dodatna programska oprema"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:291
msgid ""
"To install it automatically from your persistent storage when starting Tails."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:293
msgid "Install Every Time"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:294
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:325
msgid "Install Only Once"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:300
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:330
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:371
msgid "The configuration of your additional software failed."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:321
msgid ""
"To install it automatically when starting Tails, you can create a persistent "
"storage and activate the <b>Additional Software</b> feature."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:324
msgid "Create Persistent Storage"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:332
msgid "Creating your persistent storage failed."
msgstr ""

#. Translators: Don't translate {packages}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:341
#, python-brace-format
msgid "You could install {packages} automatically when starting Tails"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:344
msgid ""
"To do so, you need to run Tails from a USB stick installed using <i>Tails "
"Installer</i>."
msgstr ""

#. Translators: Don't translate {packages}, it's a placeholder and will be
#. replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:359
#, fuzzy, python-brace-format
msgid "Remove {packages} from your additional software?"
msgstr "Vaša dodatna programska oprema"

#. Translators: Don't translate {packages}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:363
#, python-brace-format
msgid "This will stop installing {packages} automatically."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:365
msgid "Remove"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:366
#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:119
#: config/chroot_local-includes/usr/local/bin/tor-browser:46
msgid "Cancel"
msgstr "Opusti"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:544
msgid "Installing your additional software from persistent storage..."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:546
msgid "This can take several minutes."
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:559
#, fuzzy
msgid "The installation of your additional software failed"
msgstr "Vaša dodatna programska oprema"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:574
msgid "Additional software installed successfully"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:594
msgid "The check for upgrades of your additional software failed"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:596
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:604
#, fuzzy
msgid ""
"Please check your network connection, restart Tails, or read the system log "
"to understand the problem."
msgstr ""
"Nadgradnja ni uspela. To je lahko zaradi težav v omrežju. Preverite omrežno "
"povezavo, poskusite ponovno zagnati Sledi, ali pa preberite sistemski "
"dnevnik za boljše razumevanje problema."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:603
#, fuzzy
msgid "The upgrade of your additional software failed"
msgstr "Vaša dodatna programska oprema"

#: config/chroot_local-includes/usr/local/lib/tails-additional-software-notify:37
#, fuzzy
msgid "Documentation"
msgstr "Dokumentacija Sledi"

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:96
#, python-brace-format
msgid ""
"Remove {package} from your additional software? This will stop installing "
"the package automatically."
msgstr ""

#. Translators: Don't translate {pkg}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:107
#, python-brace-format
msgid "Failed to remove {pkg}"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:124
#, fuzzy
msgid "Failed to read additional software configuration"
msgstr "Vaša dodatna programska oprema"

#. Translators: Don't translate {package}, it's a placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:154
#, python-brace-format
msgid "Stop installing {package} automatically"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:179
msgid ""
"To do so, install some software using <a href=\"synaptic.desktop\">Synaptic "
"Package Manager</a> or <a href=\"org.gnome.Terminal.desktop\">APT on the "
"command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:188
msgid ""
"To do so, unlock your persistent storage when starting Tails and install "
"some software using <a href=\"synaptic.desktop\">Synaptic Package Manager</"
"a> or <a href=\"org.gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:198
msgid ""
"To do so, create a persistent storage and install some software using <a "
"href=\"synaptic.desktop\">Synaptic Package Manager</a> or <a href=\"org."
"gnome.Terminal.desktop\">APT on the command line</a>."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:206
msgid ""
"To do so, install Tails on a USB stick using <a href=\"tails-installer."
"desktop\">Tails Installer</a> and create a persistent storage."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-additional-software-config:253
#, fuzzy
msgid "[package not available]"
msgstr "ni primeren"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Sinhronizacija sistemske ure"

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor potrebuje točno uro za pravilno delovanje, predvsem za Skrite storitve. "
"Prosimo, počakajte ..."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Sinhronizacija ure ni bila uspešna!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:124
msgid "This version of Tails has known security issues:"
msgstr "Ta verzija Sledi ima znane varnostne izhode:"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:134
msgid "Known security issues"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "Omrežna kartra ${nic} onemogočena"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, fuzzy, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Sleparjenje MAC mrežne kartice neuspešno ${nic_name} ($ {nic}), tako da je "
"začasno onemogočeno.\n"
"Morda vam je ljubše, da resetirate Sledi in onemogočite sleparjenje MAC. "
"Glejte < href='file:///usr/share/doc/tails/website/doc/first_steps/"
"startup_options/mac_spoofing.en.html'>dokumentacijo</a>."

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "Vsa omrežja onemogočena"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, fuzzy, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"Sleparjenje MAC omrežne kartice neuspešno ${nic_name} (${nic}). Okrevanje po "
"napaki tudi neuspešno, zato so vsa omrežja onemogočena\n"
"Morda vam je ljubše, da resetirate Sledi in onemogočite sleparjenje MAC. "
"Glejte <a href='file:///usr/share/doc/first_steps/startup_options/"
"mac_spoofing.en.html'>dokumentacijo</a>."

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:110
msgid "Lock Screen"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:125
msgid "Screen Locker"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:131
msgid "Set up a password to unlock the screen."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:136
msgid "Password"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:142
msgid "Confirm"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:35
#, fuzzy
msgid ""
"\"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual\""
msgstr ""
"<b>Ni dovolj spomina za preverjanje nadgradnje.</b>\n"
"\n"
"Prepričajte se, da  ta sistem ustreza zahtevam za zagon Sledi.\n"
"Glejte datoteko:///usr/share/doc/tails/website/doc/about/requirements.en."
"html\n"
"\n"
"Poskusite resetirati Sledi za ponovno preverjanje nadgradnje.\n"
"\n"
"Ali izvedite nadgradnjo ročno\n"
"Glejte https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:72
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "napaka:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:73
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Napaka"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:71
msgid "Warning: virtual machine detected!"
msgstr "Opozorilo: Zaznan virtualni stroj!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:74
#, fuzzy
msgid "Warning: non-free virtual machine detected!"
msgstr "Opozorilo: Zaznan virtualni stroj!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:77
#, fuzzy
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"Operacijski sistem gostitelja in programska oprema za virtualizacijo lahko "
"spremljata, kaj počnete v Sledi."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:81
#, fuzzy
msgid "Learn more"
msgstr "Več o Sledi"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Tor is not ready"
msgstr "Tor ni pripravljen"

#: config/chroot_local-includes/usr/local/bin/tor-browser:44
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor ni pripravljen. Zaženem Tor brskalnik vseeno?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:45
msgid "Start Tor Browser"
msgstr "Zagon Tor brskalnik"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:40
msgid "Tor"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/torstatus@tails.boum.org/extension.js:55
msgid "Open Onion Circuits"
msgstr ""

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Resnično želite zagnati nezanesljiv Brskalnik?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
#, fuzzy
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"Omrežne aktivnosti z nezanesljivim Brskalnikom <b>niso anonimne</b>. Le v "
"nujnih primerih uporabite nezanesljiv Brskalnik, npr.: če se morate "
"prijaviti ali registrirati za aktiviranje omrežne povezave."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "Zagon nezanesljivega Brskalnika ..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "To lahko traja, zato bodite potrpežjivi."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "Ugašanje nezanesljivega Brskalnika .."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"To lahko traja in ne smete ponoviti zagona nezanesljivega Brskalnika dokler "
"ni pravilno ugasnjen."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "Ponoven zagon Tor-a neuspešen. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Nezanesljiv Brskalnik"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:91
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Drugi nezanesljiv Brskalnik se trenutno izvaja ali se čisti. Poskusite malo "
"kasneje. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:99
msgid "Failed to setup chroot."
msgstr "Neuspešna nastavitev chroot."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:104
#, fuzzy
msgid "Failed to configure browser."
msgstr "Ponoven zagon Tor-a neuspešen. "

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:110
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"Noben DNS server ni bil pridobljen iz DHCP ali ročno nastavljen v "
"NetworkManager."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:121
#, fuzzy
msgid "Failed to run browser."
msgstr "Ponoven zagon Tor-a neuspešen. "

#. Translators: Don't translate {volume_label} or {volume_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:58
#, python-brace-format
msgid "{volume_label} ({volume_size})"
msgstr ""

#. Translators: Don't translate {partition_name} or {partition_size},
#. they are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:63
#, python-brace-format
msgid "{partition_name} ({partition_size})"
msgstr ""

#. Translators: Don't translate {volume_size}, it's a placeholder
#. and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:68
#, python-brace-format
msgid "{volume_size} Volume"
msgstr ""

#. Translators: Don't translate {volume_name}, it's a placeholder and
#. will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:107
#, python-brace-format
msgid "{volume_name} (Read-Only)"
msgstr ""

#. Translators: Don't translate {partition_name} and {container_path}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:115
#, python-brace-format
msgid "{partition_name} in {container_path}"
msgstr ""

#. Translators: Don't translate {volume_name} and {path_to_file_container},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:122
#, python-brace-format
msgid "{volume_name} – {path_to_file_container}"
msgstr ""

#. Translators: Don't translate {partition_name} and {drive_name}, they
#. are placeholders and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:128
#, python-brace-format
msgid "{partition_name} on {drive_name}"
msgstr ""

#. Translators: Don't translate {volume_name} and {drive_name},
#. they are placeholders and will be replaced. You should only have to translate
#. this string if it makes sense to reverse the order of the placeholders.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:135
#, python-brace-format
msgid "{volume_name} – {drive_name}"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:222
msgid "Wrong passphrase or parameters"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:224
msgid "Error unlocking volume"
msgstr ""

#. Translators: Don't translate {volume_name} or {error_message},
#. they are placeholder and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume.py:228
#, python-brace-format
msgid ""
"Couldn't unlock volume {volume_name}:\n"
"{error_message}"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:83
msgid "No file containers added"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_list.py:98
msgid "No VeraCrypt devices detected"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:114
msgid "Container already added"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:115
#, python-format
msgid "The file container %s should already be listed."
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:131
msgid "Container opened read-only"
msgstr ""

#. Translators: Don't translate {path}, it's a placeholder  and will be replaced.
#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:133
#, python-brace-format
msgid ""
"The file container {path} could not be opened with write access. It was "
"opened read-only instead. You will not be able to modify the content of the "
"container.\n"
"{error_message}"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:138
msgid "Error opening file"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:160
msgid "Not a VeraCrypt container"
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:161
#, python-format
msgid "The file %s does not seem to be a VeraCrypt container."
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:163
#, fuzzy
msgid "Failed to add container"
msgstr "Ponoven zagon Tor-a neuspešen. "

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:164
#, python-format
msgid ""
"Could not add file container %s: Timeout while waiting for loop setup.Please "
"try using the <i>Disks</i> application instead."
msgstr ""

#: config/chroot_local-includes/usr/local/lib/python3/dist-packages/unlock_veracrypt_volumes/volume_manager.py:209
msgid "Choose File Container"
msgstr ""

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Sporočite napako"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Dokumentacija Sledi"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Učenje uporabe Sledi"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Več o Sledi"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor Iskalnik"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonimni web brskalnik"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Brskajte po spletu brez anonimnosti"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Nezanesljiv web brskalnik"

#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:1
msgid "Unlock VeraCrypt Volumes"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/unlock-veracrypt-volumes.desktop.in.h:2
msgid "Mount VeraCrypt encrypted file containers and devices"
msgstr ""

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:1
#, fuzzy
msgid "Additional Software"
msgstr "Vaša dodatna programska oprema"

#: ../config/chroot_local-includes/usr/share/applications/org.boum.tails.additional-software-config.desktop.in.h:2
msgid ""
"Configure the additional software installed from your persistent storage "
"when starting Tails"
msgstr ""

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "posebna orodja Sledi"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.root-terminal.policy.in.h:1
msgid "To start a Root Terminal, you need to authenticate."
msgstr ""

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:1
#, fuzzy
msgid "Remove an additional software package"
msgstr "Vaša dodatna programska oprema"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.additional-software.policy.in.h:2
msgid ""
"Authentication is required to remove a package from your additional software "
"($(command_line))"
msgstr ""

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:61
msgid "File Containers"
msgstr ""

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:80
msgid "_Add"
msgstr ""

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:86
msgid "Add a file container"
msgstr ""

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:103
msgid "Partitions and Drives"
msgstr ""

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/main.ui.in:121
msgid ""
"This application is not affiliated with or endorsed by the VeraCrypt project "
"or IDRIX."
msgstr ""

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/volume.ui.in:38
msgid "Lock this volume"
msgstr ""

#: ../config/chroot_local-includes/usr/share/unlock-veracrypt-volumes/ui/volume.ui.in:61
msgid "Detach this volume"
msgstr ""

#: ../config/chroot_local-includes/usr/local/share/mime/packages/unlock-veracrypt-volumes.xml.in.h:1
msgid "TrueCrypt/VeraCrypt container"
msgstr ""

#~ msgid "OpenPGP encryption applet"
#~ msgstr "Odpri PGP šifrirni programček"

#~ msgid "Exit"
#~ msgstr "Izhod"

#~ msgid "About"
#~ msgstr "Vizitka"

#~ msgid "Encrypt Clipboard with _Passphrase"
#~ msgstr "Šifriranje odložišča z _Geslom za ključe"

#~ msgid "Sign/Encrypt Clipboard with Public _Keys"
#~ msgstr "Vpis / Šifriranje odložišča z javnimi _Ključi"

#~ msgid "_Decrypt/Verify Clipboard"
#~ msgstr "_Dešifriranje/Preverite Odložišče"

#~ msgid "_Manage Keys"
#~ msgstr "_Upravljanje s Ključi"

#~ msgid "The clipboard does not contain valid input data."
#~ msgstr "Odložišče ne vsebuje veljavnih vhodnih podatkov"

#~ msgid "Unknown Trust"
#~ msgstr "Neznan Skrbnik"

#~ msgid "Marginal Trust"
#~ msgstr "Mejni Skrbnik"

#~ msgid "Full Trust"
#~ msgstr "Zaupanja vreden Skrbnik"

#~ msgid "Ultimate Trust"
#~ msgstr "Dokončni Skrbnik"

#~ msgid "Name"
#~ msgstr "Ime"

#~ msgid "Key ID"
#~ msgstr "Ključ ID"

#~ msgid "Status"
#~ msgstr "Stanje"

#~ msgid "Fingerprint:"
#~ msgstr "Prstni odtis:"

#~ msgid "User ID:"
#~ msgid_plural "User IDs:"
#~ msgstr[0] "Uporbnik ID:"
#~ msgstr[1] "Uporabnika ID:"
#~ msgstr[2] "Uporabniki ID:"
#~ msgstr[3] "User IDs:"

#~ msgid "None (Don't sign)"
#~ msgstr "Nobeden (Brez podpisa)"

#~ msgid "Select recipients:"
#~ msgstr "Izbira prejemnikov:"

#~ msgid "Hide recipients"
#~ msgstr "Skrij prejemnike"

#~ msgid ""
#~ "Hide the user IDs of all recipients of an encrypted message. Otherwise "
#~ "anyone that sees the encrypted message can see who the recipients are."
#~ msgstr ""
#~ "Skrij ID uporabnika vsem prejemnikom šifriranega sporočila. Drugače lahko "
#~ "vsak, ki šifrirano sporočilo vidi, ve kdo je prejemnik."

#~ msgid "Sign message as:"
#~ msgstr "Podpiši sporočilo kot:"

#~ msgid "Choose keys"
#~ msgstr "Izberite ključe"

#~ msgid "Do you trust these keys?"
#~ msgstr "Ali zaupate tem ključem?"

#~ msgid "The following selected key is not fully trusted:"
#~ msgid_plural "The following selected keys are not fully trusted:"
#~ msgstr[0] "Sledeči izbran ključ ni vreden popolnega zaupanja:"
#~ msgstr[1] "Sledeča izbrana ključa nista vredna popolnega zaupanja:"
#~ msgstr[2] "Sledeči izbrani ključi niso vredni popolnega zaupanja:"
#~ msgstr[3] "Sledeči izbrani ključi niso vredni popolnega zaupanja:"

#~ msgid "Do you trust this key enough to use it anyway?"
#~ msgid_plural "Do you trust these keys enough to use them anyway?"
#~ msgstr[0] "Ali dovolj zaupate temu ključu, da ga vseeno uporabite?"
#~ msgstr[1] "Ali dovolj zaupate tema ključema, da ju vseeno uporabite?"
#~ msgstr[2] "Ali dovolj zaupate tem ključem, da jih vseeno uporabite?"
#~ msgstr[3] "Ali dovolj zaupate tem ključem, da jih vseeno uporabite?"

#~ msgid "No keys selected"
#~ msgstr "Ključ ni izbran"

#~ msgid ""
#~ "You must select a private key to sign the message, or some public keys to "
#~ "encrypt the message, or both."
#~ msgstr ""
#~ "Izbrati morate zasebni ključ za podpisovanje sporočila, ali kateri javni "
#~ "ključi za šifriranje sporočila, ali pa oboje."

#~ msgid "No keys available"
#~ msgstr "Ni uporabnega ključa"

#~ msgid ""
#~ "You need a private key to sign messages or a public key to encrypt "
#~ "messages."
#~ msgstr ""
#~ "Rabite zasebni ključ za podpis sporočila ali javni ključ za šifriranje le "
#~ "tega."

#~ msgid "GnuPG error"
#~ msgstr "GnuPG napaka"

#~ msgid "Therefore the operation cannot be performed."
#~ msgstr "Zato ni mogoče izvesti operacijo."

#~ msgid "GnuPG results"
#~ msgstr "GnuPG rezultati"

#~ msgid "Output of GnuPG:"
#~ msgstr "Izhod iz GnuPG:"

#~ msgid "Other messages provided by GnuPG:"
#~ msgstr "Druga sporočila ponujena od GnuPG:"

#~ msgid "Shutdown Immediately"
#~ msgstr "Nemudoma Ugasni"

#~ msgid "Reboot Immediately"
#~ msgstr "Nemudoma znova zaženi"

#~ msgid "The upgrade was successful."
#~ msgstr "Nadgradnja je bila uspešna."

#~ msgid "Network connection blocked?"
#~ msgstr "Omrežna povezava blokirana?"

#~ msgid ""
#~ "It looks like you are blocked from the network. This may be related to "
#~ "the MAC spoofing feature. For more information, see the <a href=\\"
#~ "\"file:///usr/share/doc/tails/website/doc/first_steps/startup_options/"
#~ "mac_spoofing.en.html#blocked\\\">MAC spoofing documentation</a>."
#~ msgstr ""
#~ "Izgleda, da vas blokira omrežje. To se lahko nanaša na funkcijo MAC "
#~ "sleparjenje. Za več informacij si oglejte < href=\\\"file:///usr/share/"
#~ "doc/tails/website/doc/first_steps/startup_options/mac_spoofing.en."
#~ "html#blocked\\\">dokumentacija MAC sleparjenje </a>."

#~ msgid ""
#~ "<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
#~ "virtualization.en.html'>Learn more...</a>"
#~ msgstr ""
#~ "<a href='file:///usr/share/doc/tails/website/doc/advanced_topics/"
#~ "virtualization.en.html'>Več ...</a>"

#~ msgid "I2P failed to start"
#~ msgstr "I2P se ni zagnal"

#~ msgid ""
#~ "Something went wrong when I2P was starting. Check the logs in /var/log/"
#~ "i2p for more information."
#~ msgstr ""
#~ "Pri zagonu I2P je nekaj narobe. Preverite dnevnik log v /var/log/i2p za "
#~ "nadaljne informacije"

#~ msgid "I2P's router console is ready"
#~ msgstr "I2P konzola usmerjevalnika je pripravljena"

#~ msgid "You can now access I2P's router console on http://127.0.0.1:7657."
#~ msgstr "Konzolo I2P usmerjevalnika lahko dosegate na http://127.0.0.1:7657."

#~ msgid "I2P is not ready"
#~ msgstr "I2P ni propravljen"

#~ msgid ""
#~ "Eepsite tunnel not built within six minutes. Check the router console at "
#~ "http://127.0.0.1:7657/logs or the logs in /var/log/i2p for more "
#~ "information. Reconnect to the network to try again."
#~ msgstr ""
#~ "Eepsite predor ni zgrajena v šestih minutah. Preverite usmerjevalnik "
#~ "konzolo na http://127.0.0.1:7657/logs ali dnevnike v / var / log / i2p za "
#~ "več informacij. Ponovno se priklopite na omrežje in poskusite znova."

#~ msgid "I2P is ready"
#~ msgstr "I2P je pripravljen"

#~ msgid "You can now access services on I2P."
#~ msgstr "sedaj lahko dostopate do storitev I2P"

#~ msgid "Anonymous overlay network browser"
#~ msgstr "Brskalnik anonimnega prikrivanja"

#~ msgid "I2P Browser"
#~ msgstr "I2P brskalnik"

#~ msgid "Reboot"
#~ msgstr "Ponoven zagon"

#~ msgid "Immediately reboot computer"
#~ msgstr "Nemudoma ponovno zaženite računalnik"

#~ msgid "Immediately shut down computer"
#~ msgstr "Nemudoma ugasnite računalnik"
