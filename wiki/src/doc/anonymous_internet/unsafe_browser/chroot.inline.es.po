# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2018-01-17 16:52+0000\n"
"PO-Revision-Date: 2016-07-15 07:20-0000\n"
"Last-Translator: Tails developers <amnesia@boum.org>\n"
"Language-Team: Spanish "
"<https://translate.tails.boum.org/projects/tails/trust/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If you download files using the <span class=\"application\">Unsafe\n"
"Browser</span> it is not possible to access them outside of the <span\n"
"class=\"application\">Unsafe Browser</span> itself.</p>\n"
msgstr ""
"<p>Si bajas archivos utilizando el <span class=\"application\">Unsafe\n"
"Browser</span> no puedes accederlos fuera del <span\n"
"class=\"application\">Unsafe Browser</span>.</p>\n"
